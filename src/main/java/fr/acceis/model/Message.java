package fr.acceis.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="Message")
public class Message {
	
	@Id
	@GeneratedValue
	private long Id;
	
	private String texte;
	
	@OneToOne
	@JoinColumn(name="numeroEtudiant")
	private Etudiant etudiant;
	
	@ManyToOne
	@JoinColumn(name="professeur_id")
	private Professeur professeur;

	public long getId() {
		return Id;
	}

	public void setId(long id) {
		Id = id;
	}

	public Etudiant getEtudiant() {
		return etudiant;
	}

	public void setEtudiant(Etudiant etudiant) {
		this.etudiant = etudiant;
	}

	public Professeur getProfesseur() {
		return professeur;
	}

	public void setProfesseur(Professeur professeur) {
		this.professeur = professeur;
	}

	public String getTexte() {
		return texte;
	}

	public void setTexte(String texte) {
		this.texte = texte;
	}
	
	
	
}
