package fr.acceis.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name ="Matiere")
public class Matiere {

	@Id
	@GeneratedValue
	private long Id;

	private String nom;

	@OneToMany(mappedBy = "matiere")
	private List<Cours> cours;

	@ManyToMany
	@JoinTable(name="cursus_matiere", 
	joinColumns=@JoinColumn(name="matieres_id"),
	inverseJoinColumns=@JoinColumn(name="cursus_id"))
	private List<Cursus> cursus;

	public List<Cursus> getCursus() {
		return cursus;
	}

	public void setCursus(List<Cursus> cursus) {
		this.cursus = cursus;
	}

	public List<Cours> getCours() {
		return cours;
	}

	public void setCours(List<Cours> cours) {
		this.cours = cours;
	}

	public long getId() {
		return Id;
	}

	public void setId(long id) {
		Id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}


}
