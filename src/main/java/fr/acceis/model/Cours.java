package fr.acceis.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Cours")
public class Cours {
	
	@Id
	@GeneratedValue
	private long Id;
	
	@ManyToMany
	@JoinTable(name="cours_professeur", 
		joinColumns=@JoinColumn(name="cours_id"),
		inverseJoinColumns=@JoinColumn(name="professeurs_id"))
	private List<Professeur> professeur;
	
	@ManyToOne
	@JoinColumn(name="matiere_id")
	private Matiere matiere;
	
	@OneToOne
	@JoinColumn(name="creneau_id")
	private Creneau creneau;

	
	public Matiere getMatiere() {
		return matiere;
	}

	public void setMatiere(Matiere matiere) {
		this.matiere = matiere;
	}

	public Creneau getCreneau() {
		return creneau;
	}

	public void setCreneau(Creneau creneau) {
		this.creneau = creneau;
	}

	public long getId() {
		return Id;
	}

	public void setId(long id) {
		Id = id;
	}

	public List<Professeur> getProfesseur() {
		return professeur;
	}

	public void setProfesseur(List<Professeur> professeur) {
		this.professeur = professeur;
	}

	
	
}
