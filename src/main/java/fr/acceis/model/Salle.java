package fr.acceis.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Salle")
public class Salle {

	private String nom;
	@Id
	private long Id;
	
	@OneToMany(mappedBy = "salle")
	private List<Creneau> creneau;

	public List<Creneau> getCreneau() {
		return creneau;
	}

	public void setCreneau(List<Creneau> creneau) {
		this.creneau = creneau;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public long getId() {
		return Id;
	}

	public void setId(long id) {
		Id = id;
	}
	
	
}
