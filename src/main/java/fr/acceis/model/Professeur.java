package fr.acceis.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Professeur")
public class Professeur {
	
	private String nom;
	
	private String prenom;
	
	@Id
	@GeneratedValue
	private long Id;
	
	@ManyToMany
	@JoinTable(name="cours_professeur", 
	joinColumns=@JoinColumn(name="professeurs_id"),
	inverseJoinColumns=@JoinColumn(name="cours_id"))
	private List<Cours> cours;
	
	@OneToMany(mappedBy="professeur")
	private Message message;
	

	public List<Cours> getCours() {
		return cours;
	}

	public void setCours(List<Cours> cours) {
		this.cours = cours;
	}

	public long getId() {
		return Id;
	}

	public void setId(long id) {
		Id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	
	
}
