package fr.acceis.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Creneau")
public class Creneau {

	@Id
	@GeneratedValue
	private long Id;
	
	@OneToOne
	@JoinColumn(name="cours_id")
	private Cours cours;
	
	@ManyToOne
	@JoinColumn(name="salle_id")
	private Salle salle;
	
	@ManyToOne
	@JoinColumn(name="horaire_id")
	private Horaire horaire;

	public Horaire getHoraire() {
		return horaire;
	}

	public void setHoraire(Horaire horaire) {
		this.horaire = horaire;
	}

	public long getId() {
		return Id;
	}

	public void setId(long id) {
		Id = id;
	}

	public Cours getCours() {
		return cours;
	}

	public void setCours(Cours cours) {
		this.cours = cours;
	}

	public Salle getSalle() {
		return salle;
	}

	public void setSalle(Salle salle) {
		this.salle = salle;
	}
	
	
}
