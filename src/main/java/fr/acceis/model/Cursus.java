package fr.acceis.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Cursus")
public class Cursus {

	@Id
	@GeneratedValue
	private long Id;

	private String nom;

	@OneToMany(mappedBy ="cursus")
	private List<Etudiant> etudiant;

	@ManyToMany
	@JoinTable(name="cursus_matiere", 
	joinColumns=@JoinColumn(name="cursus_id"),
	inverseJoinColumns=@JoinColumn(name="matieres_id"))
	private List<Matiere> matiere;

	public List<Matiere> getMatiere() {
		return matiere;
	}

	public void setMatiere(List<Matiere> matiere) {
		this.matiere = matiere;
	}

	public List<Etudiant> getEtudiant() {
		return etudiant;
	}

	public void setEtudiant(List<Etudiant> etudiant) {
		this.etudiant = etudiant;
	}

	public long getId() {
		return Id;
	}

	public void setId(long id) {
		Id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}


}
