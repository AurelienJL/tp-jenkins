package fr.acceis.jpa.test.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connecting {

	private static Connection connexion;

	private Connecting() {
		super();
	}
	//singleton
	public static Connection getInstance() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {

		if (connexion == null) {
			Class.forName("org.hsqldb.jdbcDriver").newInstance();
			connexion = DriverManager.getConnection("jdbc:hsqldb:data/basejpa", "sa",  "");
			return connexion;
		}
		return connexion;
	}
}
