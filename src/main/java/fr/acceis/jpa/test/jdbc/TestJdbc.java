package fr.acceis.jpa.test.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class TestJdbc {

	public static void main(String[] args) {

		try {
			listerEtudiants();
			listerProfesseurs();
			listerSalles();
			cursusEtudiant("21002127");
			salleCours(67);
			listerCoursSalle("i57");
			//		listerEtudiantsCours(67);
			//		listerProfesseursCursus(10);
			//		listerProfesseursMatiere(2);
			//		listerProfsEtudiant("21002127");
			//		emploiDuTempsSalle("i52");
			//		emploiDuTempsEtudiant("21002128");
			//		emploiDuTempsProfesseur(55);
			//		renvoyer("21002128");
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace(); 
		} catch (SQLException e) {
			e.printStackTrace();
		}


	}

	//	Liste les étudiants
	private static void listerEtudiants() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {

		Connection c = Connecting.getInstance();
		Statement stmt = c.createStatement();
		ResultSet result = stmt.executeQuery("SELECT * FROM Etudiant");
		System.out.println("Liste des étudiants :");

		while (result.next()) {
			System.out.print("{" + result.getString("nom") + " " + result.getString("prenom")+ "} \t");
		} System.out.println("\n");
	}

	//	Liste les professeurs
	private static void listerProfesseurs() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		Connection c = Connecting.getInstance();
		Statement stmt = c.createStatement();
		ResultSet result = stmt.executeQuery("SELECT * FROM Professeur");
		System.out.println("Listes des profs :");

		while (result.next()) {
			System.out.print("{" + result.getString("nom") + " " + result.getString("prenom") + "} \t");
		} System.out.println("\n");
	}


	//	Liste les salles
	private static void listerSalles() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		Connection c = Connecting.getInstance();
		Statement stmt = c.createStatement();
		ResultSet result = stmt.executeQuery("SELECT * FROM Salle");
		System.out.println("Listes des salles :");

		while (result.next()) {
			System.out.print("{" +result.getString("nom")+ "} \t");
		} System.out.println("\n");

	}

	//	Affiche le nom du cursus d'un étudiant
	private static void cursusEtudiant(String numeroEtudiant) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		String query = "SELECT nom FROM cursus INNER JOIN etudiant ON cursus.id = etudiant.cursus_id WHERE etudiant.numeroEtudiant=?";
		
		Connection c = Connecting.getInstance();
		PreparedStatement stmt = c.prepareStatement(query);
		stmt.setString(1, numeroEtudiant);
		ResultSet result = stmt.executeQuery();
		
		while (result.next()) {
			System.out.println("Cursus :");
			System.out.println("{" + result.getString("nom")+ "} \t");
		} System.out.println("\n");
	}

	//	Affiche le nom de la salle dans laquelle a lieu un cours
	private static void salleCours(long idCours) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {

		String query = "SELECT nom FROM Salle INNER JOIN Creneau ON Salle.id = Creneau.salle_id WHERE Creneau.cours_id=?";

		Connection c = Connecting.getInstance();
		PreparedStatement stmt = c.prepareStatement(query);
		stmt.setLong(1, idCours);
		ResultSet result = stmt.executeQuery();

		while (result.next()) {
			System.out.println("Salle :");
			System.out.println("{" + result.getString("nom")+ "} \t");
		} System.out.println("\n");
	}

	//	Affiche le nom des cours ayant lieu dans une salle
	private static void listerCoursSalle(String nomSalle) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {

		String query = "SELECT nom FROM matiere "
				+ "INNER JOIN Cours ON Matiere.id = Cours.Matiere_id "
				+ "INNER JOIN Creneau ON Cours.id = Creneau.cours_id "
				+ "INNER JOIN Salle ON Creneau.salle_id = salle.id "
				+ "WHERE Salle.nom=?";
		
		Connection c = Connecting.getInstance();
		PreparedStatement stmt = c.prepareStatement(query);
		stmt.setString(1, nomSalle);
		ResultSet result = stmt.executeQuery();
		System.out.println("Cours :");
		while (result.next()) {
			
			System.out.println("{" + result.getString("nom")+ "} \t");
		}
	}





	//	Affiche le nom des étudiants qui assistent à un cours
	private static void listerEtudiantsCours(long idCours) {

		/*String query = "SELECT nom, prenom, numeroEtudiant FROM Etudiant "
		+ "INNER JOIN Cursus ON cursus.id = etudiant.cursus_id "
		+ "INNER JOIN Matiere ON cursus.id = matiere.id"*/
			
	}

	//	Affiche le nom des professeurss qui enseignent dans un cursus
	private static void listerProfesseursCursus(long idCursus) {

	}

	//	Affiche le nom des professeurs qui enseignent une matière
	private static void listerProfesseursMatiere(long idMatiere) {

	}

	//	Affiche des profs qui enseignent à un étudiant
	private static void listerProfsEtudiant(String numeroEtudiant) {

	}






	//	Affiche l'emploi du temps d'une salle
	private static void emploiDuTempsSalle(String nomSalle) {

	}

	//	Affiche l'emploi du temps d'un étudiant
	private static void emploiDuTempsEtudiant(String numeroEtudiant) {

	}

	//	Affiche l'emploi du temps d'un professeur
	private static void emploiDuTempsProfesseur(long idProfesseur) {

	}

	// Renvoie un étudiant
	private static void renvoyer(String numeroEtudiant) {

	}

}
