package fr.acceis.jpa.test.jpa;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.query.Query;

import fr.acceis.model.Cours;
import fr.acceis.model.Creneau;
import fr.acceis.model.Cursus;
import fr.acceis.model.Etudiant;
import fr.acceis.model.Matiere;
import fr.acceis.model.Message;
import fr.acceis.model.Professeur;
import fr.acceis.model.Salle;

public class TestJpa {

	public static void main(String[] args) {

		ajouterEtudiant("Banner", "Bruce", "121212");
		renvoyer("123456");
		listerMessages(55);
		listerEtudiants();
		listerProfesseurs();
		listerSalles();
		cursusEtudiant("21002127");
		salleCours(67);
		listerCoursSalle("i57");
		listerEtudiantsCours(67);
		listerProfesseursCursus(10);
		listerProfesseursMatiere(2);
		listerProfsEtudiant("21002127");
		emploiDuTempsSalle("i52");
		emploiDuTempsEtudiant("21002128");
		emploiDuTempsProfesseur(55);

		HibernateUtil.getInstance().close();
	}

	//	Liste les étudiants
	private static void listerEtudiants() {
		Session s = HibernateUtil.getInstance();
		CriteriaBuilder criteriaBuilder = s.getCriteriaBuilder();
		CriteriaQuery<Etudiant> query = criteriaBuilder.createQuery(Etudiant.class);
		Root<Etudiant> root = query.from(Etudiant.class);
		query.select(root);
		Query<Etudiant> q = s.createQuery(query); //liste de type etudiant
		List<Etudiant> listEtudiants = q.getResultList();

		for (Etudiant etudiant : listEtudiants) {
			System.out.println("Last Name :" + etudiant.getNom() + " First Name : "+ etudiant.getPrenom() + " NumeroEtudiant : " + etudiant.getNumeroEtudiant());
		}
	}

	private static void ajouterEtudiant(String nom, String prenom, String numeroEtudiant) {
		Session s = HibernateUtil.getInstance();
		s.beginTransaction();

		Etudiant e = new Etudiant();
		e.setNom(nom);
		e.setPrenom(prenom);
		e.setNumeroEtudiant(numeroEtudiant);

		s.save(e);

		s.getTransaction().commit();
	}

	//	Liste les professeurs
	private static void listerProfesseurs() {
		Session s = HibernateUtil.getInstance();
		CriteriaBuilder criteriaBuilder = s.getCriteriaBuilder();
		CriteriaQuery<Professeur> query = criteriaBuilder.createQuery(Professeur.class);
		Root<Professeur> root = query.from(Professeur.class);
		query.select(root);
		Query<Professeur> q = s.createQuery(query);
		List<Professeur> listProfesseurs = q.getResultList();

		for (Professeur prof : listProfesseurs) {
			System.out.println("Last Name :" + prof.getNom() + " First Name :"+ prof.getPrenom() + " enseigne : " + prof.getCours().get(0).getMatiere());
		}
	}

	//	Liste les salles
	private static void listerSalles() {
		Session s = HibernateUtil.getInstance();
		CriteriaBuilder criteriaBuilder = s.getCriteriaBuilder();
		CriteriaQuery<Salle> query = criteriaBuilder.createQuery(Salle.class);
		Root<Salle> root = query.from(Salle.class);
		query.select(root);
		Query<Salle> q = s.createQuery(query); 
		List<Salle> listSalle = q.getResultList();

		for (Salle salle : listSalle) {
			System.out.println("Nom :" + salle.getNom());
		}
	}

	//	Affiche le nom du cursus d'un étudiant
	private static void cursusEtudiant(String numeroEtudiant) {
		Session s = HibernateUtil.getInstance();
		Etudiant e = s.load(Etudiant.class, numeroEtudiant);
		System.out.println("Nom " + e.getNom() + " Prenom "+ e.getPrenom() + "est dans le cursus" + e.getCursus().getNom());
	}

	//	Affiche le nom de la salle dans laquelle a lieu un cours
	private static void salleCours(long idCours) {
		Session s = HibernateUtil.getInstance();
		Cours cours = s.load(Cours.class, idCours);
		System.out.println("Le cours de " + cours.getMatiere().getNom() + " a lieu dans la salle " + cours.getCreneau().getSalle().getNom());
	}

	//	Affiche le nom des cours ayant lieu dans une salle
	private static void listerCoursSalle(String nomSalle) {
		Session s = HibernateUtil.getInstance();
		CriteriaBuilder criteriaBuilder = s.getCriteriaBuilder();
		CriteriaQuery<Salle> criteria = criteriaBuilder.createQuery(Salle.class);
		Root<Salle> root = criteria.from(Salle.class);
		criteria.where(criteriaBuilder.equal(root.get("nom"), nomSalle));
		Query<Salle> q = s.createQuery(criteria);
		List<Salle> listSalle = q.getResultList();
		for (Salle salle : listSalle) {
			for (Creneau creneau : salle.getCreneau()) {
				System.out.println("Dans la salle :" + salle.getNom() + " ont lieu les cours de " + creneau.getCours().getMatiere().getNom());
			}
		}
	}

	//	Affiche le nom des étudiants qui assistent à un cours
	private static void listerEtudiantsCours(long idCours) {
		Session s = HibernateUtil.getInstance();
		Cours cours = s.load(Cours.class, idCours);
		for (Cursus cursus : cours.getMatiere().getCursus()) {
			for (Etudiant etudiant : cursus.getEtudiant()) {
				System.out.println("Les etudiants qui participent au cours de " + cours.getMatiere().getNom() + " sont " + etudiant.getNom()+ " "+ etudiant.getPrenom());
			}
		}
	}

	//	Affiche le nom des professeurs qui enseignent dans un cursus
	private static void listerProfesseursCursus(long idCursus) {
		Session s = HibernateUtil.getInstance();
		Cursus cursus = s.load(Cursus.class, idCursus);
		for (Matiere matiere : cursus.getMatiere()) {
			for (Cours cours : matiere.getCours()) {
				for (Professeur professeur : cours.getProfesseur())
					System.out.println("Les profs qui enseignent dans le cursus " + cursus.getNom() + " sont : " + professeur.getNom() + " " + professeur.getPrenom());
			}
		}
	}
	//	Affiche le nom des professeurs qui enseignent une matière
	private static void listerProfesseursMatiere(long idMatiere) {
		Session s = HibernateUtil.getInstance();
		Matiere matiere = s.load(Matiere.class, idMatiere);
		for (Cours cours : matiere.getCours()) {
			for (Professeur professeur : cours.getProfesseur()) {
				System.out.println("Les profs qui enseignent la matiere : " + matiere.getNom() + " sont : " + professeur.getNom() + " " + professeur.getPrenom());
			}
		}

	}

	//	Affiche des profs qui enseignent à un étudiant
	private static void listerProfsEtudiant(String numeroEtudiant) {
		HashSet<Professeur> hashset = new HashSet<Professeur>();

		Session s = HibernateUtil.getInstance();
		Etudiant etudiant = s.load(Etudiant.class, numeroEtudiant);
		Cursus cursus = etudiant.getCursus();
		for (Matiere matiere : cursus.getMatiere()) {
			for (Cours cours : matiere.getCours()) {
				for (Professeur professeur : cours.getProfesseur()) {
					hashset.add(professeur);
				}
			}
		}
		for (Iterator iterator = hashset.iterator(); iterator.hasNext();) {
			Professeur professeur = (Professeur) iterator.next();
			System.out.println(etudiant.getPrenom() + " " + etudiant.getNom() + " suit les cours de " + professeur.getPrenom() + " "+ professeur.getNom());
		}
	}

	//	Affiche l'emploi du temps d'une salle
	private static void emploiDuTempsSalle(String nomSalle) {
		Session s = HibernateUtil.getInstance();
		CriteriaBuilder criteriaBuilder = s.getCriteriaBuilder();
		CriteriaQuery<Salle> criteria = criteriaBuilder.createQuery(Salle.class);
		Root<Salle> root = criteria.from(Salle.class);
		criteria.where(criteriaBuilder.equal(root.get("nom"), nomSalle));
		Query<Salle> q = s.createQuery(criteria);
		List<Salle> listSalle = q.getResultList();
		for (Salle salle : listSalle) {
			for (Creneau creneau : salle.getCreneau()) {
				SimpleDateFormat formater = new SimpleDateFormat("dd\\MM\\yyyy à HH:mm");
				System.out.println("Dans la salle " + salle.getNom() + " , il y a cours du " + formater.format(creneau.getHoraire().getDebut()) + " au " + formater.format(creneau.getHoraire().getFin()));
			}
		}
	}

	//	Affiche l'emploi du temps d'un étudiant
	private static void emploiDuTempsEtudiant(String numeroEtudiant) {
		HashSet<Creneau> hashset = new HashSet<Creneau>();

		Session s = HibernateUtil.getInstance();
		Etudiant etudiant = s.load(Etudiant.class, numeroEtudiant);
		Cursus cursus = etudiant.getCursus();
		for (Matiere matiere : cursus.getMatiere()) {
			for (Cours cours : matiere.getCours()) {
				Creneau creneau = cours.getCreneau();
				hashset.add(creneau);
			}
		}
		for (Iterator iterator = hashset.iterator(); iterator.hasNext();) {
			Creneau creneau = (Creneau) iterator.next();
			SimpleDateFormat formater = new SimpleDateFormat("dd\\MM\\yyyy à HH:mm");
			System.out.println("L'etudiant " + etudiant.getPrenom() + " " + etudiant.getNom() + " a cours du " + formater.format(creneau.getHoraire().getDebut()) + " au " + formater.format(creneau.getHoraire().getFin()));
		}
	}

	//	Affiche l'emploi du temps d'un professeur
	private static void emploiDuTempsProfesseur(long idProfesseur) {
		Session s = HibernateUtil.getInstance();
		Professeur professeur = s.load(Professeur.class, idProfesseur);
		for (Cours cours : professeur.getCours()) {
			Creneau creneau = cours.getCreneau();
			SimpleDateFormat formater = new SimpleDateFormat("dd\\MM\\yyyy à HH:mm");
			System.out.println("Le prof " + professeur.getPrenom() + " " + professeur.getNom() + " enseigne du " + formater.format(creneau.getHoraire().getDebut()) + " au " + formater.format(creneau.getHoraire().getFin()));
		}
	}

	// Renvoie un étudiant
	private static void renvoyer(String numeroEtudiant) {
		HashSet<Professeur> hashset = new HashSet<Professeur>();
		//ArrayList<Message> listMessage = new ArrayList<Message>();
		Session s = HibernateUtil.getInstance();
		s.beginTransaction();

		Message message = new Message();
		Etudiant etudiant = s.load(Etudiant.class, numeroEtudiant);
		if (etudiant != null) {
			s.delete(etudiant);
		}
		Cursus cursus = etudiant.getCursus();
		for (Matiere matiere : cursus.getMatiere()) {
			for (Cours cours : matiere.getCours()) {
				for (Professeur professeur : cours.getProfesseur()) {
					hashset.add(professeur);
				}
			}
		}
		for (Iterator iterator = hashset.iterator(); iterator.hasNext();) {
			Professeur professeur = (Professeur) iterator.next();
			message.setTexte(etudiant.getPrenom() + " "+ etudiant.getPrenom() + "a été renvoyé");
			message.setProfesseur(professeur);
			System.out.println(message);
			//listMessage.add(message);
			s.save(message);
		}
		s.save(etudiant);

		s.getTransaction().commit();
	}
	
	// Liste des messages
	private static void listerMessages(long idProfesseur) {
		Session s = HibernateUtil.getInstance();
		Professeur professeur = s.load(Professeur.class, idProfesseur);
		//for (Message message : lis)
		System.out.println(professeur.getMessage()); 
		
	}
}
